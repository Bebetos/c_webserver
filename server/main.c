#include "main_functions.h"
#include "header.h"

char memoryfolder[32];
struct circularBuffer *cb;

//pthread_mutex_t general;

/*
 * Function: main(listener)
 * ----------------------------
 *   Listens for connections on the port parameter.
 *
 *	 Parameters:
 *   NONE.
 *
 *   Returns: EXIT_FAILURE for error. Otherwise it
 *   doesn't return.
 */
int main(void){


	struct stat s;

	int faviconFD = getFaviconFD();
	dim_fav = getFileSize(faviconFD);
	fstat(faviconFD, &s);
	favicon = mmap(NULL, s.st_size, PROT_READ, MAP_SHARED, faviconFD, 0);

	int listenersocket,     // The listening socket
	    handlersocket,      // The handler socket
	    count;

	static struct sockaddr_in server_addr; // Server address structure

	threadpool *pool;
	pthread_t tid;

	int port = readPort();

	// Create the listener socket.
	if ((listenersocket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("Error in listen().\n");
		exit(EXIT_FAILURE);
	}

    // Make the listener socket ready for reuse
    // otherwise you have to wait until the OS kill
    // the old listener socket
	int yes = 1;
	if (setsockopt(listenersocket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes))
			== -1) {
		perror("Error in setsockopt() <- reuse address.\n");
		exit(EXIT_FAILURE);
	}

    // Populate server_addr structure.
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(port);

    // Bind the listener socket.
	if (bind(listenersocket, (struct sockaddr *) &server_addr,	sizeof(server_addr)) < 0) {
		perror("Error in bind().\n");
		exit(EXIT_FAILURE);
	}

    // Set the listener socket as passive listener with specified queue size (backlog).
	if (listen(listenersocket, LISTENER_QUEUE_SIZE) < 0) {
		perror("Error in listen().\n");
		exit(EXIT_FAILURE);
	}

	printf("Socket retrieve success\n");
	printf("PORT: %d\n", port);

//	pthread_mutex_init(&general, NULL);

    // Build the thread pool
    pool = create_threadpool();

    // Set circular buffer for comunication
    cb = initializeCircularBuffer();

    //Start circularBufferManager thread
    if(pthread_create(&tid, NULL, circularBufferManager, (void*)cb) !=0)
    			return EXIT_FAILURE;

    //Start Cache
	struct image_array_tile* array_pointer;
	array_pointer = initialize_cache();
	aging_system_start(array_pointer, cb);

    // Main listener loop.  An infinite loop that accepts connections on the listener socket
    // and creates a handler socket for each accepted connection.  The handler socket is
    // passed to a handler function.
    for(count = 1; ; count++)
    {
        // Accept a connection from the listener and create a new socket for it.
        if((handlersocket = accept(listenersocket, (struct sockaddr *) NULL, NULL)) > 0)
        	 add_connection(pool, handlersocket);

    }

	free(pool);
    return EXIT_FAILURE;
}


int getFaviconFD(){

	char* parsed_config_path;
	char* final_path;

	parsed_config_path = strdup(readFolder());

	final_path = malloc(sizeof(char)*(strlen(parsed_config_path)+strlen("/favicon.ico")));

	if(final_path==NULL){
		fprintf(stderr, "\ngetFaviconFD: Error allocating final_path");
		exit(EXIT_FAILURE);
	}
	strncat(final_path, parsed_config_path, strlen(parsed_config_path));
	strncat(final_path, "/favicon.ico", strlen("/favicon.ico"));

	//fprintf(stderr, "\ngetFaviconFD: %s", final_path);

	int fd = open(final_path , O_CREAT | O_RDONLY, S_IRWXU);
	if (fd < 0){
		fprintf(stderr, "\ngetFaviconFD: Error in open()");
		exit(EXIT_FAILURE);
	}

	return fd;
}
