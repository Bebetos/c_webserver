#include "header.h"



/*
 * Function: openFile
 * ----------------------------
 * 	Normal open call with errors check.
 *
 * 	Parameters:
 * 	resource: A string with the requested resource
 * 	path: A string with the path of the file to open.
 *
 * 	Returns:
 * 	On success it returns the file descriptor.
 */
int openFile(char *resource,char *path) {

	int fd;
	char *complete_path = NULL;

	fprintf(stderr, "OpenFile Path: %s\n", path);
	fprintf(stderr, "OpenFile resource: %s\n", resource);

	// Check if it's home request
	if (strlen(resource) <= 1) {

		complete_path = malloc(sizeof(char) * (strlen(path) + strlen("/index.html") + 1));
		complete_path[0] = '\0';

		fprintf(stderr, "COMPLETE PATH HOMEG: %s\n", complete_path);

		strncat(complete_path, path, strlen(path));
		strncat(complete_path, "/index.html", strlen("/index.html"));

		fd = open(complete_path, O_RDONLY);
		if(fd == -1){
			perror("Error in openFile() <-- open index.html\n");
			exit(EXIT_FAILURE);
		}

		free(complete_path);
		return fd;
	} else { // otherwise image is requested

		complete_path = malloc(sizeof(char) * (strlen(path) + strlen(resource) + 1));
		complete_path[0] = '\0';

		strncat(complete_path, path, strlen(path));
		strncat(complete_path, resource, strlen(resource));

		fprintf(stderr, "COMPLETE PATH IMG: %s\n", complete_path);

		fd = open(complete_path, O_RDONLY);
		if(fd == -1){
			perror("Error in openFile() <-- img.jpg\n");
			exit(EXIT_FAILURE);
		}

		free(complete_path);
		return fd;
	}
}

/*
 * Function: getFileSize
 * ----------------------------
 * 	Get the file size for the HTTP response using the
 * 	structure stat for file information.
 *
 * 	Parameters:
 * 	fd: The file descriptor
 *
 * 	Returns:
 * 	The file size.
 */
int getFileSize(int fd) {

	struct stat file_stat;

	if (fstat(fd, &file_stat) == -1) {
		fprintf(stderr, "Error in fstat() getFileSize.\n");
		perror("Error in getFileSize:");
		exit(EXIT_FAILURE);
	}

	return (int) file_stat.st_size;
}

/*
 * Function: allocateStruct
 * ----------------------------
 *   Allocate a dedicated structure for HTTP request.
 *
 *	 Parameters:
 *	 NOTHING
 *
 *   Returns:
 *   The malloc'd pointer for httpRequest structure.
 */
struct httpRequest *allocateStruct() {

	struct httpRequest *p;
	p = malloc(sizeof(struct httpRequest));
	if (p == NULL) {
		perror("Error in allocateStruct().\n");
		exit(EXIT_FAILURE);
	}
	return p;
}
/*
 * Function: parseLine
 * ----------------------------
 * 	 Parse a string and return the line.
 * 	 Used for gather information from HTTP request.
 *
 *	 Parameters:
 *	 request: The string to parse.
 *
 *   Returns:
 *   The first line of the string.
 */
char *parseLine(char *request) {

	int i = 0;
	char *p;
	p = malloc(sizeof(char) * (strlen(request)+1));
	if (p == NULL) {
		perror("Error in parseLine().\n");
		exit(EXIT_FAILURE);
	}

	while (1) {
		p[i] = request[i];
		if (request[i] == '\n') {
			p[i] = '\0';
			break;
		}
		i++;
	}
	return p;
}

/*
 * Function: parseRequest
 * ----------------------------
 *   Parse a string with HTTP request for gather the information
 *   for httpRequest structure.
 *
 *	 Parameters:
 *	 request: The complete HTTP request.
 *
 *   Returns:
 *   The structure httpRequest with all the information.
 *   	httpRequest->type: {GET, HEAD, POST}
 *   	httpRequest->connection: {close, keep-alive}
 *   	httpRequest->resource: the requested resource
 *   	httpRequest->accept: which kind of file is requested
 *   	httpRequest->user_agent: who requested the resource
 *   	httpRequest->referer: for form information
 */
struct httpRequest *parseRequest(char *request) {

	struct httpRequest *strHttp = allocateStruct();

	/* Check for HTTP Request if GET method */
	if (strstr(request, "GET")) {
		strncpy(strHttp->type, "GET", strlen("GET\0"));
	} else if (strstr(request, "HEAD")) {
		strncpy(strHttp->type, "HEAD", strlen("HEAD\0"));
	} else {
		strncpy(strHttp->type, "NTSP", strlen("NTSP\0"));
	}

	/* Check for HTTP Request if keep-alive or close */
	if (strstr(request, "keep-alive")) {
		strncpy(strHttp->connection, "keep-alive", strlen("keep-alive\0"));
	} else if (strstr(request, "close")) {
		strncpy(strHttp->connection, "close", strlen("close\0"));
	} else {
		strncpy(strHttp->connection, "ERROR!", strlen("ERROR\0"));
	}

	/* Check for HTTP Request: Resource filed */
	char *tempResource = strstr(request, "GET");
	if (tempResource == NULL) {
		strncpy(strHttp->resource, "NULL", strlen("NULL\0"));
	} else {
		tempResource = tempResource + 4;
		strncpy(strHttp->resource, parseLine(tempResource),
				strlen(parseLine(tempResource)) - 10);
		strncat(strHttp->resource, "\0",1);
	}

	/* Check for HTTP Request: Accept field */
	char *tempAccept = strstr(request, "Accept:");
	if (tempAccept == NULL) {
		strncpy(strHttp->accept, "NULL", strlen("NULL\0"));
	} else {
		tempAccept = tempAccept + 8;
		char *pA = parseLine(tempAccept);
		strncpy(strHttp->accept, pA, strlen(pA));
	}

	/* Check for HTTP Request: User-Agent field */
	char *tempUserAgent = strstr(request, "User-Agent:");
	if (tempUserAgent == NULL) {
		strncpy(strHttp->user_agent, "NULL", strlen("NULL\0"));
	} else {
		tempUserAgent = tempUserAgent + 12;
		char *pU = parseLine(tempUserAgent);
		strncpy(strHttp->user_agent, pU, strlen(pU));
	}

	/* Check for HTTP Request: Referer field */
	/* Available only when client ask for a compressed image */
	char *tempReferer = strstr(request, "Referer:");
	if (tempReferer != NULL) {
		tempReferer = tempReferer + 8;
		strncpy(strHttp->referer, parseLine(tempReferer), strlen(parseLine(tempReferer)));
	} else {
		strncpy(strHttp->referer, "NULL", strlen("NULL\0"));
	}

	return strHttp;
}

/*
 * Function: printfHttpStruct
 * ----------------------------
 *   Print the complete structure in standard
 *   output for debugging.
 *
 *	 Parameters:
 *   p: The structure httpRequest to printf.
 *
 *   Returns:
 *   NOTHING
 */
void printfHttpStructure(struct httpRequest *p) {

	printf("\nParsed HTTP request: \n");
	printf("TYPE:%s\n", p->type);
	printf("RESOURCE:%s\n", p->resource);
	printf("CONNECTION:%s\n", p->connection);
	printf("ACCEPT:%s\n", p->accept);
	printf("USER_AGENT:%s\n", p->user_agent);
	printf("REFERER:%s\n", p->referer);
}

/*
 * Function: getTypeForResponse
 * ----------------------------
 *   Get the MIME of the sending file for HTTP response.
 *
 *	 Parameters:
 *	 accept: which file the client had requested
 *	 resource: which file we are sending
 *
 *   Returns:
 *   The MIME the server have to send to the client.
 */
char *getTypeForResponse(char *accept) {

	char *p;
	p = malloc(sizeof(char) * 32);
	if (p == NULL) {
		perror("Error in getTypeForResponse().\n");
		exit(EXIT_FAILURE);
	}

	if(strstr(accept, "text/html") != NULL){
		bzero(p,32);
		strncpy(p, "text/html; charset=UTF-8", strlen("text/html; charset=UTF-8"));
		return p;
	}else if(strstr(accept, "/jpg") != NULL){
		bzero(p,32);
		strncpy(p, "image/jpg", strlen( "image/jpg"));
		return p;
	} else if(strstr(accept, "*/*")){
		bzero(p,32);
		strncpy(p, "image/jpg", strlen( "image/jpg"));
		return p;
	}else{
		bzero(p,32);
		strncpy(p, "text/html;", strlen("text/html;"));
		return p;
	}

}

/*
 * Function: sendResponseHeader
 * ----------------------------
 *   Sends the header part of the response.
 *
 *	 Parameters:
 *   resourceName: The path of the resource.
 *   contentType: The mime type for the content.
 *   responseSize: The size of the response.
 *   socket: The socket to send the response to.
 *
 *   Returns:
 *   NOTHING
 */
void sendResponseHeader(char *contentType, int responseSize, int socket)
{
	char response[BUFSIZE];

	char dateAndTime[29];
	getStrTime(dateAndTime);

	// Craft response for a file
	sprintf(response,
			"HTTP/1.1 200 OK\n"
			"Server: ImageCompressionServer\n"
			"Content-Type: %s\n"
			"Content-Length: %d\n"
			"Date: %s\n"
			"Connection: close\n\n",
			contentType, responseSize, dateAndTime);

	//printf("RISPOSTA HTTP:\n%s\n", response);

	if(write(socket, response, strlen(response)) == -1){
		perror("Error in write() <- HTTP Response\n");
		exit(EXIT_FAILURE);
	}
}







void sendImgData(void* ptr, int socket, int filesize)
{

	int sent = 0;
	int result;

	while(sent<filesize){
		result = write(socket, ptr+sent, filesize-sent);
		if(result<0){
			if(errno != EPIPE){
				perror("Error in write() <--EPIPE\n");
				exit(EXIT_FAILURE);
			}
		}
		sent+=result;
	}

}






/*
 * Function: sendData
 * ----------------------------
 *   Sends data to the socket.
 *
 *	 Parameters:
 *   fd: The file descriptor.
 *   socket: The socket to send the data to.
 *
 *   Returns:
 *   NOTHING
 */
void sendData(int fd, int socket)
{
	char buffer[BUFSIZE];
	bzero(buffer, BUFSIZE);
	int tot = 0;

	int dim = getFileSize(fd);

	while (1) {
		// First read file in chunks of 8096 bytes
		unsigned char imgBuff[BUFSIZE] = { 0 };
		int nread = read(fd, imgBuff, BUFSIZE);
		tot += nread;

		// If read was success, send data.
		if (nread > 0) {
			// Send data
			if (write(socket, imgBuff, nread) == -1) {
				if(errno != EPIPE){
				perror("Error in write() <--EPIPE\n");
				exit(EXIT_FAILURE);
				}
			}
		}
		// Checks if end of file is reached
		if (tot >= dim) {
			break;
		}
	}
}

/*
 * Function: processGet
 * ----------------------------
 *   Call to process GET requests
 *
 *	 Parameters:
 *   socket: The socket to send data out to.
 *   requestData: The data from the request.
 *   path: A string with the path of the file to open.
 */
void processGet(int socket, char *requestData,char *path)
{
	int dim;
	int fd;
	struct httpRequest *p = parseRequest(requestData);

//
//	struct stat s;
//
//	int faviconFD = getFaviconFD();
//	dim_fav = getFileSize(faviconFD);
//	fstat(faviconFD, &s);
//	favicon = mmap(NULL, s.st_size, PROT_READ, MAP_SHARED, faviconFD, 0);

	//printf("%s\n", requestData);
	//printfHttpStructure(p);

	// Check if requested homepage or a converted image
	// Requested the converted image
	if (strstr(p->resource, "compress") != NULL) {
		struct imgInfo *imgInfo = parseResource(p->resource);
		struct circularBuffer *cb = getSharedMemory();

		imgInfo->fd = 0;
		FILE *wurfl = openFileWurlf("structOk.txt");
		imgInfo->resHeight = getHeight(wurfl, p->user_agent);
		imgInfo->resWidth = getWidth(wurfl, p->user_agent);
		imgInfo->socket = socket;

		inputRequestInBuffer(cb, imgInfo);


		//printfImgInfoStructure(imgInfo);

		fclose(wurfl);


	} else {
		//Requested homepage and images


		if(strstr(p->resource, "fav")!=NULL){
			sendImgData(favicon, socket, dim_fav);

//			if (close(socket) == -1)
//				perror("Error in close() <-socket\n");

			return;
		}

		fd = openFile(p->resource, path);
		dim = getFileSize(fd);

		char *type = getTypeForResponse(p->accept);

		// Bad request
		if (type == NULL)
			sendError(socket, 400);

		sendResponseHeader(type, dim, socket);
		sendData(fd, socket);

		if(close(fd) == -1)
			perror("Error in close() <-web page\n");

//			if (close(socket) == -1)
//				perror("Error in close() <-socket\n");

	}
}
