#include "header.h"

/*
 * Struct that holds the mutual exclusion lock, threads, and queue for
 * the threadpool.
 */

/*
 * Function: allocate_threadpool
 * ----------------------------
 * 	Malloc'd the threadpool structure with errors check.
 *
 * 	Parameters:
 * 	NOTHING
 *
 * 	Returns:
 * 	On success it returns the pointer to the
 * 	malloc'd threadpool structure.
 */
struct threadpool *allocate_threadpool(){

	struct threadpool *p;
	p = (threadpool *)malloc(sizeof(threadpool));
	if(p == NULL){
		perror("Error in allocate_threadpool().\n");
		exit(EXIT_FAILURE);
	}
	return p;
}

/*
 * Function: allocate_pthreads
 * ----------------------------
 * 	Malloc'd some pthread_t.
 *
 * 	Parameters:
 * 	number: The number of pthread to create
 *
 * 	Returns:
 * 	On success it returns the malloc'd pointer to the
 * 	pthread_t dynamic array.
 */
pthread_t *allocate_pthreads(int number){

	pthread_t *p;
	p = (pthread_t *) malloc(sizeof(pthread_t) * number);
	if(p == NULL){
		perror("Error in allocate_pthreads().\n");
		exit(EXIT_FAILURE);
	}
	return p;
}

/*
 * Function: allocate_connectionqueue
 * ----------------------------
 * 	Malloc'd an integer dynamic array for connection queue.
 *
 * 	Parameters:
 * 	number: The length of the connection queue
 *
 * 	Returns:
 * 	On success it returns the malloc'd pointer to the
 * 	integer dynamic array.
 */
int *allocate_connectionqueue(int number){

	int *p;
	p = (int *) malloc(sizeof(int) * number);
	if(p == NULL){
		perror("Error in allocate_conncetionqueue().\n");
		exit(EXIT_FAILURE);
	}
	return p;
}

/*
 * Function: create_threadpool
 * ----------------------------
 * 	Create the threadpool with all the pthreads in waiting status.
 *
 * 	Parameters:
 * 	NOTHING
 *
 * 	Returns:
 * 	It returns a pointer to the threadpool.
 */
threadpool *create_threadpool() {

	threadpool *pool;	//the threadpool
	int i;

	// Allocate memory
	pool = allocate_threadpool();
	pool->threads = allocate_pthreads(MAX_THREADS);
	pool->connection_queue = allocate_connectionqueue(QUEUE_SIZE);
	pool->path = readFolder();

	// Initialize components
	pool->queue_head = 0;
	pool->queue_tail = 0;
	pool->connection_count = 0;
	pthread_mutex_init(&(pool->thread_lock), NULL);
	pthread_cond_init(&(pool->signal), NULL);

	// Build the threads
	for (i = 0; i < MAX_THREADS; i++)
	{
		// Create the new thread, send into waiting status
		if(pthread_create(&(pool->threads[i]), NULL, worker_thread, (void*) pool) !=0)
			return NULL;
	}

	// Return the complete thread pool
	return pool;
}

/*
 * Function: worker_thread
 * ----------------------------
 * 	Wait for a connection and when one arrives picks one and
 * 	process the connection.
 *
 * 	Parameters:
 * 	t_pool: The pointer of our threadpool
 *
 * 	Returns:
 * 	NEVER
 */
void *worker_thread(void *t_pool) {

	threadpool *pool = (threadpool *) t_pool;
	int connection;

	for (;;) {

		//
		while (pool->connection_count == 0) {
			pthread_cond_wait(&(pool->signal), &(pool->thread_lock));
		}

		// Get the first connection from the front of the queue
		connection = pool->connection_queue[pool->queue_head];
		pool->queue_head += 1;	//move the head to the next item in the queue

		// If the head marker was just at the last item in the queue, send
		// it back to the front of the queue.
		if (pool->queue_head == QUEUE_SIZE) {
			pool->queue_head = 0;
		}

		//subtract from the connections left to be processed
		pool->connection_count -= 1;

		// Send the connection to the router for processing
		processConnection(&connection, pool->path);

	}

	pthread_exit(NULL);

}

/*
 * Function: processConnection
 * ----------------------------
 * 	Called by a worker_thread, it process the connection reading
 * 	the HTTP request.
 *
 * 	Parameters:
 * 	socket: The connected socket pointer
 * 	path: A string with the path of the file to open.
 *
 * 	Returns:
 * 	NOTHING
 */
void *processConnection(void *socket,char *path) {

	int *sockfd = (int *) socket;

	// Variables
	long buffer_bytes;	// Number of bytes in the buffer
	static char buffer[BUFSIZE + 1];	// Buffer to hold request string

	// Receive the HTTP request, place into buffer
	buffer_bytes = recv(*sockfd, buffer, BUFSIZE, 0);

	// Check that the request is not empty and that the BUFSIZE has not been exceeded
	if (buffer_bytes <= 0 || buffer_bytes > BUFSIZE) {
		// Send error
		sendError(*sockfd, 400);
		return 0;
	}

	// Check for a valid request method is being used
	if (!strncmp(buffer, "GET ", 4)) {
		// Log GET request, check formatting of request, call process method
		processGet(*sockfd, buffer, path);
		bzero(buffer, BUFSIZE);
		return 0;
	} else {
		// Log invalid HTTP request, send error
		sendError(*sockfd, 405);
		return 0;
	}
}

/*
 * Function: add_connection
 * ----------------------------
 *   Adds a valid connection to the connection queue.
 *   When one arrives send a signal and wake a waiting thread.
 *
 *	 Parameters:
 *   pool: The threadpool
 *   socketfd: The connected socket
 *
 *   Returns: 0 if successful
 */
int add_connection(threadpool *pool, int socketfd) {

	int result = 0;
	int next;

	next = pool->queue_tail + 1;

	// If the 'next' pointer is at the end of the queue, recycle to the
	//beginning of the queue.
	if (next == QUEUE_SIZE) {
		next = 0;
	}

	do {
		// Check that we can accept another connection
		if (pool->connection_count == QUEUE_SIZE) {
			perror("The queue is full.\n");
			result = -1;
			break;
		}

		// Insert the connection into the end of the queue
		pool->connection_queue[pool->queue_tail] = socketfd;
		pool->queue_tail = next;
		pool->connection_count += 1;

		// Signal the thread pool that a connection is waiting
		pthread_cond_signal(&(pool->signal));

	} while (0);

	return result;
}
