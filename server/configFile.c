#include "header.h"

/*
 * Function: readConfigFile
 * -----------------------------
 * 	Read the configuration file "config.txt.
 *
 * 	parameters:
 * 	NOTHING
 *
 * 	returns:
 * 	A string with the file "config.txt" information.
 */
char *readConfigFile(void){

	int n;

	//Open the file
	int fd = open("config.txt", O_RDONLY);
	if(fd == -1){
		perror("Error in openConfigFile().\n");
		exit(EXIT_FAILURE);
	}

	//Read from the file
	char buffer[BUFSIZE+1];
	while ((n = read(fd, buffer, BUFSIZE)) > 0) {
		buffer[n] = '\0';
	}

	char *p = malloc(sizeof(char) * (strlen(buffer)));
	if(p == NULL){
		perror("Error in malloc().\n");
		exit(EXIT_FAILURE);
	}
	strcpy(p, buffer);
	if(close(fd) == -1)
		perror("Error in close() <- readConfigFile\n");

	return p;
}

/*
 * Function: readPort
 * ----------------------------
 * 	Read the port number from "config.txt".
 *
 * 	parameters:
 * 	NOTHING
 *
 * 	returns:
 * 	The number of the port for the socket.
 * 	On error returns a default door: 5000.
 */
int readPort(void){

	char *buffer;

	buffer = readConfigFile();

	//Looking for the string "port="
	char *tempStrPort = strstr(buffer, "port=");
	if(tempStrPort == NULL){
		perror("Port not found.\nUsing default: 5000");
		return DEFAULT_PORT;
	}else{
		char *strPort = parseLine(tempStrPort + 5);
		//printf("%s", tempStrPort);

		//Port found. Converting from string to int
		unsigned long port;
		errno = 0;
		char *p;
		port = strtoul(strPort, &p, 0);
		if (errno != 0 || *p != '\0') {
			perror("Invalid format port.\nUsing default: 5000\n");
			return DEFAULT_PORT;
		}
		return (int) port;
	}
}

/*
 * Function: readFolder
 * ----------------------------
 * 	Read the server folder from "config.txt".
 *
 * 	parameters:
 * 	NOTHING
 *
 * 	returns:
 * 	On success returns the server folder.
 * 	On error shutdown the server.
 */
char *readFolder(void){

	char *buffer;
	char *path;

	buffer = readConfigFile();
	//Looking for the string "home="
	char *tempStrPath = strstr(buffer, "folder=");
	if(tempStrPath == NULL){
		//String not found
		perror("Invalid file config.txt\nMissing field: folder=<#path>\n");
		exit(EXIT_FAILURE);
	}else{
		//String found
		path = parseLine(tempStrPath + 7);
		return path;
	}
}
