#ifndef HEADERFILE_H_
#define HEADERFILE_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <wand/MagickWand.h>

#define BUFSIZE 8096
#define MAX_THREADS 10	// number of threads to start in thread pool
#define QUEUE_SIZE  50  //number of waiting connections allowed in the queue
#define FILETYPES_ARRAY_SIZE 100 // max elements in filetypes array
#define LISTENER_QUEUE_SIZE 64 // default listener queue size
#define DEFAULT_START "index.html"	//default page to open if none provided
#define DEFAULT_PORT 5000 // default port

#define TEMP "structOk.txt" //binary file with parsed user agents from wurfl
#define nMax 350
#define nMed 60

int dim_fav;
void* favicon;

//pthread_mutex_t general;

/*
 * Struct that holds the mutual exclusion lock, threads,
 * and queue for the threadpool.
 */
typedef struct threadpool {
	pthread_mutex_t thread_lock;
	pthread_cond_t signal;
	pthread_t *threads;
	int *connection_queue;
	int queue_head;
	int queue_tail;
	int connection_count;
	char *path;
} threadpool;

/*
 * Structure for HTTP request information.
 */
typedef struct httpRequest {
	char type[8];
	char resource[128];
	char connection[16];
	char accept[128];
	char user_agent[128];
	char referer[64];
} httpRequest;

/*
 *  Structure for imgInfo gathered from HTTP request
 */
typedef struct imgInfo {
	int value;
	int resHeight;
	int resWidth;
	char file[128];
	int fd;
	void* address;
	int socket;
} imgInfo;

//circular_buffers
typedef struct circularBuffer{
    pthread_mutex_t mutex;
    pthread_cond_t signal;
    int count;
	int start;
	int end;
	imgInfo *imgInfo[MAX_THREADS];
} circularBuffer;

//main.c
int getFaviconFD();

//threadpool.c
struct threadpool *allocate_threadpool();
pthread_t *allocate_pthreads(int number);
int *allocate_connectionqueue(int number);

threadpool *create_threadpool();
void *worker_thread(void *t_pool);
void *processConnection(void *socket,char *path);
int add_connection(threadpool *pool, int socketfd);

//imgInfo.c
char *getHTTPvalue(char *resource);
struct imgInfo *parseResource(char *httpResource);
struct imgInfo *allocImgStruct();
void printfImgInfoStructure(struct imgInfo *p);
char *getFolderImg(char *filename);

//handlerHTTP.c
void sendResponseHeader(char *contentType, int responseSize, int socket);
void sendData(int fd, int socket );
void sendImgData(void* ptr, int socket, int filesize);
void processGet(int socket, char *requestData,char *path);
char *getTypeForResponse(char *accept);
int getFileSize(int fd);
struct httpRequest *allocateStruct();
char *parseLine(char *request);
void printfHttpStructure(struct httpRequest *p);


//getTime.c
void getStrTime(char *timestamp);

//handlerError.c
void sendError(int sockfd, int errorCode);
char * getMsg(int code);

//imgWand.c
char *createFileName(char *filename, int img_width, int img_heigth, int img_quality);
float getQuality(int value);
int convertImg(struct imgInfo *p);

//configFile.c
char *readFolder();
int readPort();
char *readConfigFile();

//sharedBuffer
struct circularBuffer *getSharedMemory();
struct imgInfo *allocateImgInfo(int number);
struct circularBuffer *initializeCircularBuffer();

//void initializeCircularBuffer(circularBuffer*);
void* inputRequestInBuffer(struct circularBuffer *cb, struct imgInfo *imgInf);
void *circularBufferManager(void *arg);

//inttostring.c
char *intToString(int value, char *result, int base);

//readStructParsed.c
FILE *openFileWurlf(char *path);
int getWidth(FILE *f, char ua[nMax]);
int getHeight(FILE *f, char ua[nMax]);

#endif
