#include "header.h"

/*
 * Function: getSharedMemory
 * ----------------------------
 * 	Malloc'd the circularBuffer structure with errors check.
 *
 * 	Parameters:
 * 	NOTHING
 *
 * 	Returns:
 * 	On success it returns the pointer to the
 * 	malloc'd circularBuffer structure.
 */
struct circularBuffer *getSharedMemory(){

    int sid, errno;
    key_t key = ftok("..", 'c');
    circularBuffer *p;

    sid = shmget(key, sizeof(circularBuffer), IPC_CREAT | 0666);
    if (sid == -1) {
    	fprintf(stderr, "ERRNO = %d", errno);
        perror("Error in shmget()");
        exit(EXIT_FAILURE);
    }
    p = shmat(sid, NULL, 0);
    if (p == (void *) -1) {
    	fprintf(stderr, "ERRNO = %d", errno);
        perror("Error in shmat()");
        exit(EXIT_FAILURE);
    }
    /* on creation the shared memory region is filled with zeros */
    return p;
}

/*
 * Function: initializeCircularBuffer
 * ----------------------------
 * 	MInitialize the circularBuffer structure.
 *
 * 	Parameters:
 * 	NOTHING
 *
 * 	Returns:
 * 	On success it returns the pointer to the
 * circularBuffer structure with all the info.
 */
circularBuffer *initializeCircularBuffer(){

	circularBuffer *p;
	p = getSharedMemory();
	pthread_mutex_init(&(p->mutex), NULL);
	pthread_cond_init(&(p->signal), NULL);
	p->end = 0;
	p->start = 0;
	p->count = 0;

	int i=0;
	for(i=0;i<MAX_THREADS; i++){
		p->imgInfo[i] = NULL;
	}

	return p;
}

/*
 * Function: inputRequestInBuffer
 * ----------------------------
 * 	Insert the request from client (for converting image request)
 * 	into the buffer
 *
 * 	Parameters:
 * 	cb: pointer to circularBuffer structure
 * 	imgInf: pointer to imgInfo structure
 *
 * 	Returns:
 * 	NOTHING
 */
void* inputRequestInBuffer(struct circularBuffer *cb, struct imgInfo *imgInf){

	int nE;

	/* Get mutex lock for critical area */
	if(pthread_mutex_lock(&(cb->mutex)) != 0){
		perror("Error in pthread_mutex_lock() <-- getFileDescriptor\n");
		exit(EXIT_FAILURE);
	}

	/* Critical Area */
	nE = (cb->end + 1) % MAX_THREADS;
	if(nE == 0)
		nE++;
	cb->end = nE;
	cb->count += 1;

	if(pthread_mutex_unlock(&(cb->mutex)) != 0){
		perror("Error in pthread_mutex_unlock() <-- getFileDescriptor\n");
		exit(EXIT_FAILURE);
	}

	/* check if buffer is full */
	while(nE == cb->start){
		/* full buffer */
		usleep(10000);
	}


	/* Get mutex lock for critical area */
	if(pthread_mutex_lock(&(cb->mutex)) != 0){
		perror("Error in pthread_mutex_lock() <-- getFileDescriptor\n");
		exit(EXIT_FAILURE);
	}

	cb->imgInfo[nE-1] = imgInf;

	/* Get mutex unlock after critical area */
	if(pthread_mutex_unlock(&(cb->mutex)) != 0){
		perror("Error in pthread_mutex_unlock() <-- getFileDescriptor\n");
		exit(EXIT_FAILURE);
	}

	/* check if file descriptor is null*/
	while((cb->imgInfo[nE-1]->fd) == 0){
		/* wait for cache */
		usleep(10000);
	}

	void* address = cb->imgInfo[nE-1]->address;
	cb->imgInfo[nE-1] = NULL;


	return address;
}
/*
 * Function: circularBufferManager
 * ----------------------------
 * 	Manage the start index of the buffer in a thread.
 *
 * 	Parameters:
 * 	NOTHING
 *
 * 	Returns:
 * 	NOTHING
 */
void *circularBufferManager(void *arg){

	circularBuffer *cb = (circularBuffer *) arg;

	for(;;){

		/* Critical Area */
		while(cb->start == cb->end){
			/* circular buffer empty */
			usleep(1000);
		}


		while(cb->imgInfo[cb->start] != NULL){
			/* start position not NULL*/
			usleep(1000);
		}

		cb->start = (cb->start + 1) % MAX_THREADS;
	}

	exit(EXIT_FAILURE);
}
