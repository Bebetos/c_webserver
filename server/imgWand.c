#include "header.h"

////////////////////////////////////////////////////////////////////////////////////////////////
/*		WARNING
 *
 * 		Compile with:
 * 		gcc -Wall -Wextra -O2 -pthread -o main main.c configFile.c handlerError.c handlerHTTP.c
 * 		threadpool.c getTime.c imgInfo.c imgWand.c `pkg-config --cflags --libs MagickWand`
 *
 *//////////////////////////////////////////////////////////////////////////////////////////////
/*
 * This procedure is used for handling the exception from MagickWand environment.
 * Code take from MagickWand example
 */
#define ThrowWandException(wand) \
{ \
  char \
    *description; \
 \
  ExceptionType \
    severity; \
 \
  description=MagickGetException(wand,&severity); \
  (void) fprintf(stderr,"%s %s %lu %s\n",GetMagickModule(),description); \
  description=(char *) MagickRelinquishMemory(description); \
  exit(EXIT_FAILURE); \
}

/*
 * This function is used for file a correct filename in a specific standard: filename_widthxheigth_quality.jpg
 * Maybe more extensions in future.
 */
char *createFileName(char *filename, int img_width, int img_heigth,
		int img_quality) {

	/* Cast from int to char for img_width, img_ heigth, img_quality */
	char value_width[5];
	sprintf(value_width, "%d", img_width);
	char value_heigth[5];
	sprintf(value_heigth, "%d", img_heigth);
	char value_quality[4];
	sprintf(value_quality, "%d", img_quality);

	char *newname = malloc(
			sizeof(char)
					* (strlen(readFolder()) + strlen("/converted_img/") + strlen(filename)
							+ strlen(value_width) + strlen(value_heigth)
							+ strlen(value_quality) + strlen("_x_") + 1));
	if (newname == NULL) {
		fprintf(stderr, "Error in createFileName() -> newname == NULL.\n");
		exit(EXIT_FAILURE);
	}
	newname[0] = '\0';
	strcat(newname, readFolder());
	strcat(newname, "/converted_img/");
	strcat(newname, filename);
	strcat(newname, "_");
	strcat(newname, value_width);
	strcat(newname, "x");
	strcat(newname, value_heigth);
	strcat(newname, "_");
	strcat(newname, value_quality);
	strcat(newname, ".jpg");

	printf("\n|| newName : %s \n", newname);

	return newname;
}
/*
 * This function is used for convert from int to float the quality value.
 * Also check is the value is between 0.0 and 1.0 and set it correctly if there are errors.
 */
float getQuality(int value) {

	float quality;

	quality = (float) value;
	/* quality must be between 0.0 and 1.0 */
	if (quality < 0.0)
		quality = 0.0;
	if (quality > 100.0)
		quality = 100.0;

	return quality;
}

char *originalImgPath(char *filename){

	char *path = malloc( sizeof(char)
					* (strlen(readFolder()) + strlen("/img/") + strlen(filename)
					+ strlen(".jpg") + 1));

	if (path == NULL) {
		fprintf(stderr, "Error in createFileName() -> newname == NULL.\n");
		exit(EXIT_FAILURE);
	}
	path[0] = '\0';

	strcat(path, readFolder());
	strcat(path, "/img/");
	strcat(path, filename);
	strcat(path, ".jpg");

	printf("\n|| originalPath : %s \n", path);

	return path;
}

/*
 * This function convert the requested image using library Magick/wand.h
 * It returns on success a file descriptor of the converted image already load in memory
 */
int convertImg(struct imgInfo *p) {

	if (p == NULL) {
		perror("Error in convertImg() -> NULL struct imgInfo.\n");
		exit(EXIT_FAILURE);
	}

	/* Copy image information */
	int img_heigth = p->resHeight;
	if (img_heigth == 0)
//		img_heigth = 768;
		img_heigth = 1080;

	int img_width = p->resWidth;
	if (img_width == 0)
//		img_width = 1024;
		img_width = 1920;

	float value = getQuality(p->value);
	char filename[33];
	strncpy(filename, p->file, 32);

	int memoryFd;
	MagickWand *magick_wand;
	MagickBooleanType status;

	/* Read an image */
	MagickWandGenesis();
	magick_wand = NewMagickWand(); /*  */
	status = MagickReadImage(magick_wand,originalImgPath(filename));
	if (status == MagickFalse)
		ThrowWandException(magick_wand);

	/* Convert image */
	MagickResetIterator(magick_wand);
	while (MagickNextImage(magick_wand) != MagickFalse){
		MagickResizeImage(magick_wand, img_width, img_heigth, KaiserFilter, 1);
//	MagickResizeImage(magick_wand, img_width, img_heigth, NULL, 0);
		MagickSetImageCompressionQuality(magick_wand,value);
	}

	/* Write image */
	char *convert_img = createFileName(filename, img_width, img_heigth,	(int) value);
	status = MagickWriteImages(magick_wand, convert_img, MagickTrue);
	if (status == MagickFalse)
		ThrowWandException(magick_wand);

	magick_wand = DestroyMagickWand(magick_wand);
	MagickWandTerminus();

	/*TODO*/
	//memoryFd = loadImgInMemory(convert_img);

		memoryFd = open(convert_img, O_RDONLY);
//    	memoryFd=shm_open(convert_img, O_RDONLY | O_CREAT, S_IRUSR | S_IRWXU);
	if (memoryFd == -1) {
//		fprintf(stderr, "\nERRNO: %d", errno);
		perror("Error in open() <-- converted file\n");
		exit(EXIT_FAILURE);
	}
	return memoryFd;
}
