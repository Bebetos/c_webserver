#include <time.h>
#include "header.h"

#define TIME_FORMAT "%a, %d %b %Y %H:%M:%S GMT"

/*
 * Function: getStrTime
 * ----------------------------
 * 	Used for get the local time for the HTTP
 * 	response field Date.
 *
 *  Parameters:
 *  timestamp: The pointer for the string
 *
 *  Returns:
 *  The current system time.
 *  Ex. Sun, 12 Oct 2014 15:07:10 GMT
 */
void getStrTime(char *timestamp) {
	// Declare variables.
	time_t now;
	char timebuff[100];

	// Get current system time.
	now = time((time_t*) 0);

	// Copies TIME_FORMAT into timebuff and expands format tags with GMT values.
	strftime(timebuff, sizeof(timebuff), TIME_FORMAT, gmtime(&now));

	// Copy the contents of timebuff to memory loc of timestamp.
	strncpy(timestamp, timebuff, sizeof(timebuff));

}
