#include "header.h"

/*
 * Function: allocImgStruct
 * ----------------------------
 * 	 Malloc'd a imgInfo structure and check errors.
 *
 *	 Parameters:
 *	 NOTHING
 *
 *   Returns:
 *   The malloc'd pointer to a imgInfo struct.
 */
struct imgInfo *allocImgStruct() {

	struct imgInfo *p;
	p = malloc(sizeof(struct imgInfo));
	if (p == NULL) {
		fprintf(stderr, "Error in allocateStruct().\n");
		exit(EXIT_FAILURE);
	}
	return p;
}

/*
 * Function: printfImgInfoStructure
 * ----------------------------
 *   Print the complete structure in standard
 *   output for debugging.
 *
 *	 Parameters:
 *   p: The structure imgInfo to printf.
 *
 *   Returns:
 *   NOTHING
 */
void printfImgInfoStructure(struct imgInfo *p) {

	printf("\n\nimgInformation Structure: \n");
	printf("FILE: %s\n", p->file);
	printf("IMG VALUE: %d\n", p->value);
	printf("IMG-HEIGHT: %d\n", p->resHeight);
	printf("IMG-WIDTH: %d\n", p->resWidth);
}

/*
 * Function: getHTTPvalue
 * ----------------------------
 * 	 Parse a string and return a substring
 * 	 with requested values.
 * 	 Used for gather information from HTTP resource field.
 *
 *	 Parameters:
 *	 resource: The string to parse.
 *
 *   Returns:
 *   The requested value after "substring=" #myvalue.
 */
char *getHTTPvalue(char *resource) {

	int i = 0;
	char *p;
	p = malloc(sizeof(char) * (strlen(resource)+1));
	if (p == NULL) {
		perror("Error in parseLine().\n");
		exit(EXIT_FAILURE);
	}

	while (1) {
		p[i] = resource[i];
		if (resource[i] == '&') {
			p[i++] = '\0';
			break;
		}
		i++;
	}
	return p;
}

/*
 * Function: parseResource
 * ----------------------------
 * 	 Parse a string and return the line.
 * 	 Used for gather information from HTTP request.
 *
 *	 Parameters:
 *	 request: The string to parse.
 *
 *   Returns:
 *   The first line of the string.
 */
struct imgInfo *parseResource(char *httpResource) {

	struct imgInfo *imgInf = allocImgStruct();

	/* Search for value in Resource field */
	char *tempImgValue = strstr(httpResource, "compress_value=");
	char *tempImg = strstr(httpResource, "image=");

	tempImgValue = tempImgValue + 15;
	tempImg = tempImg + 6;

	/* Parsing the value line from string to integer */
	char *p;
	errno = 0;
	int v = (int) strtol(getHTTPvalue(tempImgValue), &p, 0);
	if (errno != 0 || *p != '\0') {
		perror("Error in parseResource -> imgValue.\n");
		exit(EXIT_FAILURE);
	}

	/* Put info into imgInfo structure */
	strncpy(imgInf->file, getHTTPvalue(tempImg), 128);
	imgInf->value = v;

	return imgInf;

}

char *getFolderImg(char *filename){

	const char *folder = readFolder();
	char *path;
	path = malloc(sizeof(char) * (strlen(folder) + strlen(filename) + 5));
	if(path == NULL){
		perror("Error in malloc() <-- getFolderImg\n");
		exit(EXIT_FAILURE);
	}

	sprintf(path, "%s/img/%s.jpg", folder, filename);
	return path;
}
