### Image Web Server ###
This is a simple web server that I coded for a University project.

## Prerequisites: ##
Install ImageMagick exernal libraries with the commands:
	sudo apt-get install imagemagick
	sudo apt-get install libmagickwand-dev
	sudo apt-get install libmagickcore-dev


## How to use it: ##
1) Open the config.txt file and set the port and the folder with the web page.
2) Compile everything using the make command via console.
3) Run the server and have fun!



# NB: #
1) Do not use a default port for the connection (<= 1023) or use it and run the server with admin privilegies.
2) Do not set the folder with the web page in a privilieged path.



